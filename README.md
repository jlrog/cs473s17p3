# COMP 473 - Bridge & Observer Patterns

## Running the JAR application 

**System requirements**

* Java 8 SDK or later

From the source directory go to the location of the application target folder:
Then run the application with the java compiler:

`java -jar  target/facilities.jar`

## Design Pattern Application

On this project the Observer pattern was used to allow a classes to track their modifications by 
notifying any subscribers (Logger) when there is a state change in the class. 

A model of the facility management system was implemented utilizing the Bridge patter while the Logger Observer Patter
implementation keeps track of the changes and write them to an observer_log.txt file. It is possible to set different 
levels of notification to capture multiple changes on the entities. In that way we can write to different 
logs simultaneously depending on the nature of the change every time that there is a change on a facility.

Now the other pattern used on this project is the bridge pattern that allows to creates two different hierarchies. 
One for abstraction and another for implementation which prevents the permanent binding between abstraction and implementation
by removing the dependency between the two. The pattern creates a connection/"bridge" that it is in charge of the 
coordinating between abstraction and implementation as well as allow us to extend the two separately. This implementation 
helps to mitigate the impact of modification of the abstract class to the Client.