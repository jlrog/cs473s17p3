package edu.luc.cs473.system.contracts;

import edu.luc.cs473.system.observer.Log;
import edu.luc.cs473.system.observer.Logger;

/**
 * Created by jlroo on 4/17/17.
 *
 *  bridge pattern Implementation
 *
 * */

public class Inspection implements Details {

    public String inspectionDate;
    Logger msg = new Logger();

    public void setDate(String date){
        inspectionDate = date;
        new Log(msg);
        msg.setState(" INSPECTION DATE SET: " + date);
    }
    public String getDate(){
        return inspectionDate;
    }

    @Override
    public void desc() {
        System.out.print("Inspected on: " + this.getDate()+"\n");
    }

}