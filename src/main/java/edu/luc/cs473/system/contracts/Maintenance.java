package edu.luc.cs473.system.contracts;

import edu.luc.cs473.system.observer.Log;
import edu.luc.cs473.system.observer.Logger;

/**
 * Created by jlroo on 4/17/17.
 *
 *  bridge pattern Implementation
 *
 * */

public class Maintenance implements Details {

    public String maintenance_type;
    Logger msg = new Logger();

    public void setType(String mtype){
        maintenance_type = mtype;
        new Log(msg);
        msg.setState(" MAINTENANCE TYPE SET: " + mtype);
    }
    public String getType(){
        return maintenance_type;
    }

    @Override
    public void desc() {
        System.out.print("Maintenance Type: " + this.getType() +"\n");
    }

}
