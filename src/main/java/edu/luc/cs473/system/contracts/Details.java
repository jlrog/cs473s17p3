package edu.luc.cs473.system.contracts;

/**
 * Created by jlroo on 4/17/17.
 *
 * Implementor for bridge pattern
 *
 * */

public interface Details {
    void desc();
}
