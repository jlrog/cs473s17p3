package edu.luc.cs473.system.contracts;

import edu.luc.cs473.system.observer.Log;
import edu.luc.cs473.system.observer.Logger;

/**
 * Created by jlroo on 4/17/17.
 *
 *  bridge pattern Implementation
 *
 * */

public class Usage implements Details {

    public String hours_usage;
    Logger msg = new Logger();

    public void setUsage(String usage){
        hours_usage = usage;
        new Log(msg);
        msg.setState(" USAGE TIME SET: " + usage);

    }
    public String getUsage(){
        return hours_usage;
    }

    @Override
    public void desc() {
        System.out.print("Hours of use: " + this.getUsage()+"\n");
    }

}