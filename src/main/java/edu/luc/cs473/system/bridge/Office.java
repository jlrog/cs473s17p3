package edu.luc.cs473.system.bridge;

import edu.luc.cs473.system.contracts.Details;

/**
 * Created by jlroo on 4/17/17.
 *
 * Refine abstraction 2 in bridge pattern
 *
 */

public class Office extends Facility {

    public Office(Details details1, Details details2, Details details3) {
        super(details1, details2,details3);
    }

    @Override
    public void facilityDesc() {
        System.out.print("Facility Type: Office \n");
        details1.desc();
        details2.desc();
        details3.desc();
    }

}