package edu.luc.cs473.system.bridge;

import edu.luc.cs473.system.contracts.Details;

/**
 * Created by jlroo on 4/17/17.
 *
 * abstraction in bridge pattern
 *
 * */

abstract class Facility {
    protected Details details1;
    protected Details details2;
    protected Details details3;

    protected Facility(Details details1, Details details2, Details details3) {
        this.details1 = details1;
        this.details2 = details2;
        this.details3 = details3;
    }
    abstract public void facilityDesc();
}