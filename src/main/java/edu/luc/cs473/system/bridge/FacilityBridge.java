package edu.luc.cs473.system.bridge;

import edu.luc.cs473.system.contracts.Inspection;
import edu.luc.cs473.system.contracts.Maintenance;
import edu.luc.cs473.system.contracts.Usage;

/**
 * Created by jlroo on 4/17/17.
 */

public class FacilityBridge {

    public static void main(String[] args) {

        Inspection inspection1 =  new Inspection();
        inspection1.setDate("04-18-2017");
        Maintenance maintenance1 = new Maintenance();
        maintenance1.setType("Media");
        Usage usage1 = new Usage();
        usage1.setUsage("2300");
        Facility facility1 = new StudyRoom(inspection1,maintenance1,usage1);
        facility1.facilityDesc();

        System.out.print("--------------------------------\n");

        Inspection inspection2 =  new Inspection();
        inspection2.setDate("04-19-2017");
        Maintenance maintenance2 = new Maintenance();
        maintenance2.setType("Projector");
        Usage usage2 = new Usage();
        usage2.setUsage("5000");
        Facility facility2 = new ClassRoom(inspection2,maintenance2,usage2);
        facility2.facilityDesc();

        System.out.print("--------------------------------\n");

        Inspection inspection3 =  new Inspection();
        inspection3.setDate("04-20-2017");
        Maintenance maintenance3 = new Maintenance();
        maintenance3.setType("AC");
        Usage usage3 = new Usage();
        usage3.setUsage("1000");
        Facility facility3 = new Office(inspection3,maintenance3,usage3);
        facility3.facilityDesc();

    }
}
