package edu.luc.cs473.system.observer;

/**
 * Created by jlroo on 4/17/17.
 */

import java.util.ArrayList;

public class Logger {
    ArrayList<Observer> observers=new ArrayList<Observer>();
    String state;

    public String getState() {
        return state;
    }

    public void setState(String state){
        this.state=state;
        notifyObservers();
    }

    public void registerObserver(Observer o){
        observers.add(o);
    }

    public void removeObserver(Observer o){
        observers.remove(o);
    }


    public void notifyObservers() {
        for (Observer observer : observers){
            observer.update();
        }
    }
}

