package edu.luc.cs473.system.observer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by jlroo on 4/17/17.
 */

public class Log extends Observer {

    File file = new File("observer_Log.txt");

    public Log(Logger messenger) {
        this.message = messenger;
        this.message.registerObserver(this);
    }

    @Override
    public void update() {
        try {
            String line = "[STATUS]" + message.getState() + "\n";
            FileWriter fw = new FileWriter(file.getAbsoluteFile(),true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(line);
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}