package edu.luc.cs473.system.observer;

/**
 * Created by jlroo on 4/17/17.
 */

public abstract class Observer {
    protected Logger message;
    public abstract void update();
}